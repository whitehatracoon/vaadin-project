package de.leuphana.webshop.backend.model;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class Cart {

	private Map<Long, CartItem> cartItems;
	private int numberOfArticles;

	public Cart() {
		cartItems = new HashMap<Long, CartItem>();
		numberOfArticles = 0;
	}

	public void addCartItem(Work work) {
		Long workId = work.getId();

		CartItem cartItem;
		if (cartItems.containsKey(workId)) {
			cartItem = cartItems.get(workId);
			cartItem.incrementQuantity();
		} else {
			cartItem = new CartItem(work);
			cartItems.put(workId, cartItem);
		}
		numberOfArticles++;
	}

	public void deleteCartItem(Long workId) {
		for (CartItem cartItem : cartItems.values()) {
			if (cartItem.getWork().getId() == (workId)) {
				int quantity = cartItem.getQuantity();
				cartItems.remove(workId);
				numberOfArticles = numberOfArticles - quantity;
				break;
			}
		}
	}

	public void decrementArticleQuantity(Integer articleId) {
		if (cartItems.containsKey(articleId)) {
			CartItem cartItem = (CartItem) cartItems.get(articleId);
			cartItem.decrementQuantity();

			if (cartItem.getQuantity() <= 0)
				cartItems.remove(articleId);

			numberOfArticles--;
		}
	}

	public Collection<CartItem> getCartItems() {
		return cartItems.values();
	}

	public int getNumberOfArticles() {
		return numberOfArticles;
	}

}
package de.leuphana.webshop.backend.model;

public class Customer {

	private static Integer lastGeneratedCustomerId = 0;

	private Integer customerId;
	private Cart cart;

	public Customer(Cart cart) {
		this.customerId = ++lastGeneratedCustomerId;
		this.cart = cart;
	}

	public Integer getCustomerId() {
		return customerId;
	}

	public Cart getCart() {
		return cart;
	}
}

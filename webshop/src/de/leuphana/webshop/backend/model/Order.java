package de.leuphana.webshop.backend.model;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class Order {

	private int orderId;
	private int customerId;
	private Map<Integer, OrderPosition> orderPositions;

	public Order(int orderId) {
		this.orderId = orderId;
		orderPositions = new HashMap<Integer, OrderPosition>();
	}

	public int getCustomerId() {
		return customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	public int getOrderId() {
		return orderId;
	}

	public Collection<OrderPosition> getOrderPositions() {
		return orderPositions.values();
	}

	public void addOrderPosition(OrderPosition orderPosition) {
		orderPositions.put(orderPosition.getPositionId(), orderPosition);
	}

}
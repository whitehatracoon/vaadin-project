package de.leuphana.webshop.backend.model;

import java.io.Serializable;

public abstract class Work implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -9071967812283522247L;

	private Long id;

	private String subject;
	private int numberOfPages;
	private String year;
	private String type;
	
	public Work() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public int getNumberOfPages() {
		return numberOfPages;
	}

	public void setNumberOfPages(int numberOfPages) {
		this.numberOfPages = numberOfPages;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "Work{" + "id=" + id + ", subject=" + subject + ", number of pages=" + numberOfPages + ", year=" + year;
	}
}

package de.leuphana.webshop.backend.model;

@SuppressWarnings("serial")
public class SeminarWork extends Work{

	private String lecture;
	private String lecturer;
	
	public String getLecture() {
		return lecture;
	}
	public void setLecture(String lecture) {
		this.lecture = lecture;
	}
	public String getLecturer() {
		return lecturer;
	}
	public void setLecturer(String lecturer) {
		this.lecturer = lecturer;
	}
}

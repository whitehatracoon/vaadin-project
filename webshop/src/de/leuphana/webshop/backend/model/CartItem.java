package de.leuphana.webshop.backend.model;

public class CartItem {

	private Work work;
	private int quantity;

	public CartItem(Work work) {
		this.work = work;
		quantity = 1;
	}

	public Work getWork() {
		return work;
	}

	public int getQuantity() {
		return quantity;
	}

	public void incrementQuantity() {
		quantity++;
	}
	
	public void decrementQuantity() {
		quantity--;
	}

}
package de.leuphana.webshop.backend.model;

@SuppressWarnings("serial")
public class BachelorWork extends Work {

	private String student;
	private String examiner;
	private String secondAuditor;
	
	public String getStudent() {
		return student;
	}
	public void setStudent(String student) {
		this.student = student;
	}
	public String getExaminer() {
		return examiner;
	}
	public void setExaminer(String examiner) {
		this.examiner = examiner;
	}
	public String getSecondAuditor() {
		return secondAuditor;
	}
	public void setSecondAuditor(String secondAuditor) {
		this.secondAuditor = secondAuditor;
	}
	
	
}

package de.leuphana.webshop.backend.model;

public class OrderPosition {

	private int positionId;
	private Work work;
	private int quantity;

	public OrderPosition(int positionId) {
		this.positionId = positionId;
	}

	public Integer getPositionId() {
		return positionId;
	}

	public Work getArticle() {
		return work;
	}

	public void setWork(Work work) {
		this.work = work;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

}
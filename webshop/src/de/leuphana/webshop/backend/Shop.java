package de.leuphana.webshop.backend;

import java.util.HashMap;
import java.util.Map;

import de.leuphana.webshop.backend.model.Cart;
import de.leuphana.webshop.backend.model.Customer;
import de.leuphana.webshop.backend.model.Work;
import de.leuphana.webshop.backend.service.WorkService;

public class Shop {

	private static Shop instance;
	
	WorkService service;

	private Map<Integer, Customer> customers;

	private Shop() {
		service  = WorkService.getInstance(); 
		customers = new HashMap<Integer, Customer>();
	}

	public static Shop getInstance() {
		if (instance == null)
			instance = new Shop();
		return instance;
	}

	public WorkService getWorkService(){
		return service;
	}
	
	public Integer createCustomerWithCart() {
		Customer customer = new Customer(new Cart());

		customers.put(customer.getCustomerId(), customer);

		return customer.getCustomerId();
	}
	
	public void removeArticleFromCart(Integer customerId, Long workId) {
		customers.get(customerId).getCart().deleteCartItem(workId);
	}

	public void addArticleToCart(Integer customerId, Long workId) {
		Work foundWork = service.getWork(workId);

		customers.get(customerId).getCart().addCartItem(foundWork);
	}
	
	public Cart getCartForCustomer(Integer customerId) {
		return customers.get(customerId).getCart();
	}
}

package de.leuphana.webshop.backend.service;

import java.util.ArrayList;
import java.util.List;

import de.leuphana.webshop.backend.dao.WorkDAO;
import de.leuphana.webshop.backend.dao.WorkDAOImpl;
import de.leuphana.webshop.backend.model.Work;

public class WorkService {

	private static WorkService instance;

	WorkDAO workDAO;

	private WorkService() {
		workDAO = new WorkDAOImpl();
	}

	public static WorkService getInstance() {
		if (instance == null) {

			instance = new WorkService();
		}
		return instance;

	}

	public synchronized List<Work> findAll(String stringFilter) {
		ArrayList<Work> arrayList = new ArrayList<Work>();

		for (Work work : workDAO.findAll()) {
				boolean passesFilter = (stringFilter == null || stringFilter.isEmpty())
						|| work.toString().toLowerCase().contains(stringFilter.toLowerCase());
				if (passesFilter) {
					arrayList.add(work);
				}
		}
		return arrayList;
	}

	public synchronized long count() {
		return workDAO.findAll().size();
	}

	public synchronized void delete(Work value) {
		workDAO.deleteWork(value);
	}

	public synchronized void save(Work entry) {

		if (workDAO.findById(entry.getId()) != null) {
			workDAO.updateWork(entry);
		} else {
			workDAO.insertWork(entry);
		}

	}

	public synchronized Work getWork(Long workId) {
		return workDAO.findById(workId);
	}
}

package de.leuphana.webshop.backend.dao;

import java.util.List;

import de.leuphana.webshop.backend.model.Work;

public interface WorkDAO {

	List<Work> findAll();
    Work findById(Long workId);
    boolean insertWork(Work work);
    boolean updateWork(Work work);
    boolean deleteWork(Work work);
	
}

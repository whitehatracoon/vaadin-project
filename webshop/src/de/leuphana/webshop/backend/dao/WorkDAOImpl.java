package de.leuphana.webshop.backend.dao;

import java.util.List;

import de.leuphana.webshop.backend.dao.connector.WorkDBConnector;
import de.leuphana.webshop.backend.model.Work;

public class WorkDAOImpl implements WorkDAO {

	
	WorkDBConnector connector;

	public WorkDAOImpl() {
		connector  = new WorkDBConnector();
	}
	
	@Override
	public List<Work> findAll() {
		return connector.fetchWorks();
	}

	@Override
	public Work findById(Long workId) {
		return connector.fetchWorkById(workId);
	}

	@Override
	public boolean insertWork(Work work) {
		connector.insertWork(work);
		return true;
	}

	@Override
	public boolean updateWork(Work work) {
		connector.updateWork(work);
		return true;
	}

	@Override
	public boolean deleteWork(Work work) {
		connector.deleteWork(work);
		return true;
	}

}

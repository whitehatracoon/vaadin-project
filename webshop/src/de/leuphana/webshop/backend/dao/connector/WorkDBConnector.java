package de.leuphana.webshop.backend.dao.connector;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import de.leuphana.webshop.backend.model.BachelorWork;
import de.leuphana.webshop.backend.model.SeminarWork;
import de.leuphana.webshop.backend.model.Work;

public class WorkDBConnector {

	private Connection connection;

	// JDBC driver name and database URL
	private static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
	private static final String DB_URL = "jdbc:mysql://localhost:3306/workDb";

	// Database credentials
	private static final String USER = "workAdmin";
	private static final String PASS = "pass";

	public WorkDBConnector() {
		registerDriver();
		createConnection();
	}

	private void registerDriver() {
		try {
			Class.forName(JDBC_DRIVER).newInstance();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void createConnection() {
		try {
			connection = DriverManager.getConnection(DB_URL, USER, PASS);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public List<Work> fetchWorks() {
		List<Work> works = new ArrayList<Work>();

		try {
			Statement stmt = connection.createStatement();
			String selectAllWorks = "SELECT * FROM works";
			ResultSet resultSet = stmt.executeQuery(selectAllWorks);

			while (resultSet.next()) {
				Long workId = resultSet.getLong("workId");
				String subject = resultSet.getString("subject");
				int numberOfPages = resultSet.getInt("numberOfPages");
				String year = resultSet.getString("year");

				if (resultSet.getString("type").equals("BachelorWork")) {
					String student = resultSet.getString("student");
					String examiner = resultSet.getString("examiner");
					String secondAuditor = resultSet.getString("second auditor");

					BachelorWork bachelorWork = new BachelorWork();
					bachelorWork.setId(workId);
					bachelorWork.setSubject(subject);
					bachelorWork.setNumberOfPages(numberOfPages);
					bachelorWork.setYear(year);
					bachelorWork.setStudent(student);
					bachelorWork.setExaminer(examiner);
					bachelorWork.setSecondAuditor(secondAuditor);

					works.add(bachelorWork);
				} else if (resultSet.getString("type").equals("SeminarWork")) {
					String lecture = resultSet.getString("lecture");
					String lecturer = resultSet.getString("lecturer");

					SeminarWork seminarWork = new SeminarWork();
					seminarWork.setId(workId);
					seminarWork.setSubject(subject);
					seminarWork.setNumberOfPages(numberOfPages);
					seminarWork.setYear(year);
					seminarWork.setLecture(lecture);
					seminarWork.setLecturer(lecturer);

					works.add(seminarWork);
				}

			}

			resultSet.close();
			stmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return works;
	}

	public Work fetchWorkById(Long id) {

		try {
			Statement stmt = connection.createStatement();
			String selectWorkById = "SELECT * FROM works" + " WHERE workId = " + id + ";";
			ResultSet resultSet = stmt.executeQuery(selectWorkById);

			while (resultSet.next()) {
				Long workId = resultSet.getLong("workId");
				String subject = resultSet.getString("subject");
				int numberOfPages = resultSet.getInt("numberOfPages");
				String year = resultSet.getString("year");

				if (resultSet.getString("type").equals("BachelorWork")) {
					String student = resultSet.getString("student");
					String examiner = resultSet.getString("examiner");
					String secondAuditor = resultSet.getString("second auditor");

					BachelorWork bachelorWork = new BachelorWork();
					bachelorWork.setId(workId);
					bachelorWork.setSubject(subject);
					bachelorWork.setNumberOfPages(numberOfPages);
					bachelorWork.setYear(year);
					bachelorWork.setStudent(student);
					bachelorWork.setExaminer(examiner);
					bachelorWork.setSecondAuditor(secondAuditor);

					return bachelorWork;
				} else

				if (resultSet.getString("type").equals("SeminarWork")) {
					String lecture = resultSet.getString("lecture");
					String lecturer = resultSet.getString("lecturer");

					SeminarWork seminarWork = new SeminarWork();
					seminarWork.setId(workId);
					seminarWork.setSubject(subject);
					seminarWork.setNumberOfPages(numberOfPages);
					seminarWork.setYear(year);
					seminarWork.setLecture(lecture);
					seminarWork.setLecturer(lecturer);

					return seminarWork;
				}
			}
			resultSet.close();
			stmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return null;
	}

	public void insertWork(Work work) {
		String subject = work.getSubject();
		int numberOfPages = work.getNumberOfPages();
		String year = work.getYear();
		String type = work.getType();

		if (work instanceof BachelorWork) {
			work = (BachelorWork) work;
			String student = ((BachelorWork) work).getStudent();
			String examiner = ((BachelorWork) work).getExaminer();
			String secondAuditor = ((BachelorWork) work).getSecondAuditor();

			try {
				Statement stmt = connection.createStatement();
				String insertWork = "INSERT INTO `works` (`workId`, `subject`, `numberOfPages`, `year`, `type`, `student`, `examiner`, `second auditor`, `lecture`, `lecturer`) VALUES (NULL, '"
						+ subject + "', '" + numberOfPages + "', '" + year + "', '" + type + "', '" + student + "', '"
						+ examiner + "', '" + secondAuditor + "', NULL, NULL);";
				stmt.executeUpdate(insertWork);

				stmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		} else if (work instanceof SeminarWork) {
			work = (SeminarWork) work;
			String lecture = ((SeminarWork) work).getLecture();
			String lecturer = ((SeminarWork) work).getLecturer();

			try {
				Statement stmt = connection.createStatement();
				String insertWork = "INSERT INTO `works` (`workId`, `subject`, `numberOfPages`, `year`, `type`, `student`, `examiner`, `second auditor`, `lecture`, `lecturer`) VALUES (NULL, '"
						+ subject + "', '" + numberOfPages + "', '" + year + "', '" + type
						+ "', 'NULL', 'NULL', 'NULL', '" + lecture + "', '" + lecturer + "');";
				stmt.executeUpdate(insertWork);

				stmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public void updateWork(Work work) {
		Long id = work.getId();
		String subject = work.getSubject();
		int numberOfPages = work.getNumberOfPages();
		String year = work.getYear();

		if (work instanceof BachelorWork) {
			work = (BachelorWork) work;
			String student = ((BachelorWork) work).getStudent();
			String examiner = ((BachelorWork) work).getExaminer();
			String secondAuditor = ((BachelorWork) work).getSecondAuditor();

			try {
				Statement stmt = connection.createStatement();
				String updateWork = "UPDATE `works` SET `subject` = '" + subject + "', `numberOfPages` = '"
						+ numberOfPages + "', `year` = '" + year + "', `student` = '" + student + "', `examiner` = '"
						+ examiner + "', `second auditor` = '" + secondAuditor + "' WHERE `works`.`workId` = " + id
						+ ";";
				stmt.executeUpdate(updateWork);

				stmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		} else if (work instanceof SeminarWork) {
			work = (SeminarWork) work;
			String lecture = ((SeminarWork) work).getLecture();
			String lecturer = ((SeminarWork) work).getLecturer();

			try {
				Statement stmt = connection.createStatement();
				String updateWork = "UPDATE `works` SET `subject` = '" + subject + "', `numberOfPages` = '"
						+ numberOfPages + "', `year` = '" + year + "', `lecture` = '" + lecture + "', `lecturer` = '"
						+ lecturer + "' WHERE `works`.`workId` = " + id + ";";
				stmt.executeUpdate(updateWork);

				stmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

	}

	public void deleteWork(Work work) {
		Long id = work.getId();

		try {
			Statement stmt = connection.createStatement();
			String deleteWork = "DELETE FROM `works` WHERE `works`.`workId` = " + id + ";";
			stmt.executeUpdate(deleteWork);

			stmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}

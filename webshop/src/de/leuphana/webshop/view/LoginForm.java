package de.leuphana.webshop.view;

import java.util.Locale;
import java.util.ResourceBundle;

import com.vaadin.event.ShortcutAction;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.TextField;

public class LoginForm extends FormLayout{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4064985317660782595L;
	
	
	// i18n
	String language = "de";
	String country = "DE";

	Locale locale = new Locale(language, country);
	
	ResourceBundle captions = ResourceBundle.getBundle("de.leuphana.webshop.view.i18n.Captions", locale);
	
	// components
	Button loginButton = new Button("", this::login);
	Button cancelButton = new Button("", this::cancel);
	TextField usernameTF = new TextField();
	TextField passwordTF = new TextField();

	String username;
	String password;
	
	public LoginForm() {
		configureComponents();
		buildLayout();
	}

	private void configureComponents() {
		loginButton.setClickShortcut(ShortcutAction.KeyCode.ENTER);
		
		cancelButton.setIcon(FontAwesome.CLOSE);
		
		usernameTF.addTextChangeListener(e -> username = e.getText());
		passwordTF.addTextChangeListener(e -> password = e.getText());
		
		setVisible(false);
	}

	private void buildLayout() {
		// captions
		refreshCaptions();
		
		setSizeUndefined();
		setMargin(true);

		HorizontalLayout actions = new HorizontalLayout(loginButton, cancelButton);
		actions.setSpacing(true);
		
		addComponents(actions, usernameTF, passwordTF);

	}
	
	public void refreshCaptions() {
		loginButton.setCaption(captions.getString("loginButton"));
		cancelButton.setCaption(captions.getString("cancelButton"));
		usernameTF.setCaption(captions.getString("usernameTextField"));
		passwordTF.setCaption(captions.getString("passwordTextField"));
	}

	public void login(ClickEvent event) {
		if (username.equals(getUI().admin.getUsername()) && password.equals(getUI().admin.getPassword())) {
			setVisible(false);
			getUI().admin.setLoggedIn(true);
			getUI().refreshLayout();
			Notification.show(captions.getString("loginSuccessNotification"), Type.TRAY_NOTIFICATION);
		}
	}

	public void cancel(ClickEvent event) {
		setVisible(false);
		Notification.show(captions.getString("loginAbortNotification"), Type.TRAY_NOTIFICATION);
	}

	@Override
	public WebshopUI getUI() {
		return (WebshopUI) super.getUI();
	}
}

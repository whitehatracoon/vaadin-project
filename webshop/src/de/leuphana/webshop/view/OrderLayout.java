package de.leuphana.webshop.view;

import java.util.ResourceBundle;

import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.Table;
import com.vaadin.ui.VerticalLayout;

import de.leuphana.webshop.backend.model.Cart;
import de.leuphana.webshop.backend.model.CartItem;
import de.leuphana.webshop.backend.model.Order;
import de.leuphana.webshop.backend.model.OrderPosition;

@SuppressWarnings("serial")
public class OrderLayout extends VerticalLayout{
	
	//i18n
	ResourceBundle captions;

	private static int orderId;
	private Order order;
	private OrderPosition orderPosition;

	Button backButton = new Button();
	Button orderButton = new Button();

	public OrderLayout(Cart cart, ResourceBundle captions) {
		++orderId;
		order = new Order(orderId);

		int positionId = 1;
		for (CartItem cartItem : cart.getCartItems()) {
			orderPosition = new OrderPosition(positionId);
			orderPosition.setWork(cartItem.getWork());
			orderPosition.setQuantity(cartItem.getQuantity());
			order.addOrderPosition(orderPosition);
			positionId++;
		}

		this.captions = captions;
		
		configureComponents();
		buildLayout();
	}

	private void configureComponents() {
		backButton.addClickListener(e -> getUI().buildCartLayout());
		orderButton.addClickListener(new ClickListener() {

			@Override
			public void buttonClick(ClickEvent event) {

				removeComponent(backButton);
				removeComponent(orderButton);

				String msg = captions.getString("orderSuccessNotification")+" [orderID: " + order.getOrderId() + "]";
				Notification.show(msg, Type.TRAY_NOTIFICATION);
			}
		});

	}

	private void buildLayout() {
		//captions
		backButton.setCaption(captions.getString("backButton"));
		orderButton.setCaption(captions.getString("orderButton"));
		
		Table orderPositionsTable = new Table();
		orderPositionsTable.setCaption(captions.getString("orderPositionsTableCaption"));
		orderPositionsTable.addContainerProperty(captions.getString("orderPositionsTableQuantityProperty"), Integer.class, null);
		orderPositionsTable.addContainerProperty(captions.getString("orderPositionsTableSubjectProperty"), String.class, null);

		int i = 1;
		for (OrderPosition orderPosition : order.getOrderPositions()) {
			orderPositionsTable.addItem(
					new Object[] { orderPosition.getQuantity(), orderPosition.getArticle().getSubject() }, i++);
		}

		orderPositionsTable.setPageLength(orderPositionsTable.size());

		addComponents(backButton, orderPositionsTable, orderButton);
	}

	@Override
	public WebshopUI getUI() {
		return (WebshopUI) super.getUI();
	}

}

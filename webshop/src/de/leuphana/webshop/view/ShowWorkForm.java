package de.leuphana.webshop.view;

import java.util.Locale;
import java.util.ResourceBundle;

import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.TextField;

import de.leuphana.webshop.backend.Shop;
import de.leuphana.webshop.backend.model.BachelorWork;
import de.leuphana.webshop.backend.model.SeminarWork;
import de.leuphana.webshop.backend.model.Work;

public class ShowWorkForm extends FormLayout{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6926205184448879587L;

	Shop shop = Shop.getInstance();
	
	// i18n
	String language = "de";
	String country = "DE";

	Locale locale = new Locale(language, country);
	
	ResourceBundle captions = ResourceBundle.getBundle("de.leuphana.webshop.view.i18n.Captions", locale);

	// components
	Button add2cart = new Button("", this::add2cart);
	Button cancel = new Button("", this::cancel);

	TextField subject = new TextField();
	TextField numberOfPages = new TextField();
	TextField year = new TextField();
	TextField student = new TextField();
	TextField examiner = new TextField();
	TextField secondAuditor = new TextField();
	TextField lecture = new TextField();
	TextField lecturer = new TextField();

	Work work;

	BeanFieldGroup<Work> formFieldBindings;

	public ShowWorkForm() {
		configureComponents();
		buildLayout();
	}

	private void configureComponents() {
		subject.setReadOnly(true);
		numberOfPages.setReadOnly(true);
		year.setReadOnly(true);

		add2cart.addStyleName("friendly");
		cancel.setIcon(FontAwesome.CLOSE);

		setVisible(false);
	}

	private void buildLayout() {
		// captions
		refreshCaptions();
		
		setSizeUndefined();
		setMargin(true);

		HorizontalLayout actions = new HorizontalLayout(add2cart, cancel);

		addComponents(actions, subject, numberOfPages, year);

	}
	
	public void refreshCaptions() {
		subject.setCaption(captions.getString("subjectTextField"));
		numberOfPages.setCaption(captions.getString("numberOfPagesTextField"));
		year.setCaption(captions.getString("yearTextField"));
		add2cart.setCaption(captions.getString("add2cartButton"));
		
		student.setCaption(captions.getString("studentTextField"));
		examiner.setCaption(captions.getString("examinerTextField"));
		secondAuditor.setCaption(captions.getString("secondAuditorTextField"));
		lecture.setCaption(captions.getString("lectureTextField"));
		lecturer.setCaption(captions.getString("lecturerTextField"));
	}

	public void add2cart(ClickEvent event) {
		if (getSession().getAttribute("customerId") != null) {
			int customerId = (int) getSession().getAttribute("customerId");

			shop.addArticleToCart(customerId, work.getId());

			String msg = work.getSubject() +" "+ captions.getString("workAddedToBasketNotification");
			Notification.show(msg, Type.TRAY_NOTIFICATION);
			getUI().refreshCartButtonCaption();

		} else {
			int customerId = shop.createCustomerWithCart();
			getSession().setAttribute("customerId", customerId);

			shop.addArticleToCart(customerId, work.getId());

			String msg = work.getSubject() + " " + captions.getString("workAddedToBasketNotification");
			Notification.show(msg, Type.TRAY_NOTIFICATION);
			getUI().refreshCartButtonCaption();
		}
	}
	
	public void cancel(Button.ClickEvent event) {
		getUI().workList.select(null);
		setVisible(false);
	}

	void show(Work work) {
		this.work = work;
		
		if (work instanceof BachelorWork) {
			removeComponent(lecture);
			removeComponent(lecturer);
			addComponents(student, examiner, secondAuditor);
		} else if (work instanceof SeminarWork) {
			removeComponent(student);
			removeComponent(examiner);
			removeComponent(secondAuditor);
			addComponents(lecture, lecturer);
		}
		
		if (work != null) {
			// Bind the properties of the contact POJO to fiels in this form
			formFieldBindings = BeanFieldGroup.bindFieldsBuffered(work, this);
		}
		setVisible(work != null);
	}

	@Override
	public WebshopUI getUI() {
		return (WebshopUI) super.getUI();
	}

}

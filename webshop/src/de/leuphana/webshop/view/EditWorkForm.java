package de.leuphana.webshop.view;

import java.util.Locale;
import java.util.ResourceBundle;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.event.ShortcutAction;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Button;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.OptionGroup;
import com.vaadin.ui.TextField;
import com.vaadin.ui.themes.ValoTheme;

import de.leuphana.webshop.backend.model.BachelorWork;
import de.leuphana.webshop.backend.model.SeminarWork;
import de.leuphana.webshop.backend.model.Work;

public class EditWorkForm extends FormLayout {

	/**
	 * 
	 */
	private static final long serialVersionUID = 256731458671087602L;

	// i18n
	String language = "de";
	String country = "DE";

	Locale locale = new Locale(language, country);

	ResourceBundle captions = ResourceBundle.getBundle("de.leuphana.webshop.view.i18n.Captions", locale);

	// components
	Button save = new Button("", this::save);
	Button cancel = new Button("", this::cancel);
	TextField subject = new TextField();
	TextField numberOfPages = new TextField();
	TextField year = new TextField();
	TextField student = new TextField();
	TextField examiner = new TextField();
	TextField secondAuditor = new TextField();
	TextField lecture = new TextField();
	TextField lecturer = new TextField();

	OptionGroup typeSelection = new OptionGroup();

	Work work;

	BeanFieldGroup<Work> formFieldBindings;

	public EditWorkForm() {
		configureComponents();
		buildLayout();
	}

	@SuppressWarnings("serial")
	private void configureComponents() {
		save.setStyleName(ValoTheme.BUTTON_PRIMARY);
		save.setIcon(FontAwesome.SAVE);
		save.setClickShortcut(ShortcutAction.KeyCode.ENTER);

		cancel.setIcon(FontAwesome.CLOSE);

		typeSelection.addItems("Bachelor", "Seminar");
		typeSelection.addValueChangeListener(new ValueChangeListener() {

			@Override
			public void valueChange(ValueChangeEvent event) {
				if (typeSelection.getValue().equals("Bachelor")) {
					removeComponent(lecture);
					removeComponent(lecturer);
					addComponents(student, examiner, secondAuditor);
					work = new BachelorWork();
					work.setType("BachelorWork");
				} else if (typeSelection.getValue().equals("Seminar")) {
					removeComponent(student);
					removeComponent(examiner);
					removeComponent(secondAuditor);
					addComponents(lecture, lecturer);
					work = new SeminarWork();
					work.setType("SeminarWork");
				}
			}

		});

		setVisible(false);
	}

	private void buildLayout() {
		// captions
		refreshCaptions();

		setSizeUndefined();
		setMargin(true);

		HorizontalLayout actions = new HorizontalLayout(save, cancel);
		actions.setSpacing(true);

		addComponents(actions, subject, numberOfPages, year);
	}

	public void refreshCaptions() {
		save.setCaption(captions.getString("saveButton"));
		subject.setCaption(captions.getString("subjectTextField"));
		numberOfPages.setCaption(captions.getString("numberOfPagesTextField"));
		year.setCaption(captions.getString("yearTextField"));
		typeSelection.setCaption(captions.getString("typeSelection"));

		student.setCaption(captions.getString("studentTextField"));
		examiner.setCaption(captions.getString("examinerTextField"));
		secondAuditor.setCaption(captions.getString("secondAuditorTextField"));
		lecture.setCaption(captions.getString("lectureTextField"));
		lecturer.setCaption(captions.getString("lecturerTextField"));
	}

	public void save(Button.ClickEvent event) {
		if (year.getValue().length() == 4) {
			
			// Commit the fields from UI to DAO
			work.setSubject(subject.getValue());
			work.setNumberOfPages(Integer.parseInt(numberOfPages.getValue()));
			work.setYear(year.getValue());
			
			if (work instanceof BachelorWork) {
				work = (BachelorWork) work;
				((BachelorWork) work).setStudent(student.getValue());
				((BachelorWork) work).setExaminer(examiner.getValue());
				((BachelorWork) work).setSecondAuditor(secondAuditor.getValue());
			} else if (work instanceof SeminarWork) {
				work = (SeminarWork) work;
				((SeminarWork) work).setLecture(lecture.getValue());
				((SeminarWork) work).setLecturer(lecturer.getValue());
			}

			// Save DAO to backend with direct synchronous service API
			getUI().service.save(work);

			String msg = captions.getString("savingNotification") + " " + work.getSubject();
			Notification.show(msg, Type.TRAY_NOTIFICATION);
			getUI().refreshWorkList();
		}

		else {
			String msg = captions.getString("yearWrongSetNotification");
			Notification.show(msg, Type.TRAY_NOTIFICATION);
		}

	}

	public void cancel(Button.ClickEvent event) {
		Notification.show(captions.getString("cancellingNotification"), Type.TRAY_NOTIFICATION);
		getUI().workList.select(null);
		setVisible(false);
	}

	void create() {
		removeComponent(lecture);
		removeComponent(lecturer);
		removeComponent(student);
		removeComponent(examiner);
		removeComponent(secondAuditor);
		addComponent(typeSelection);
		setVisible(true);
	}

	void edit(Work work) {
		this.work = work;
		removeComponent(typeSelection);

		if (work instanceof BachelorWork) {
			removeComponent(lecture);
			removeComponent(lecturer);
			addComponents(student, examiner, secondAuditor);
		} else if (work instanceof SeminarWork) {
			removeComponent(student);
			removeComponent(examiner);
			removeComponent(secondAuditor);
			addComponents(lecture, lecturer);
		}

		if (work != null) {
			// Bind the properties of the contact POJO to fields in this form
			formFieldBindings = BeanFieldGroup.bindFieldsBuffered(work, this);
			subject.focus();
		}
		setVisible(work != null);
	}

	@Override
	public WebshopUI getUI() {
		return (WebshopUI) super.getUI();
	}

}

package de.leuphana.webshop.view;

import java.util.Locale;
import java.util.ResourceBundle;

import javax.servlet.annotation.WebServlet;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.Title;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.event.ItemClickEvent.ItemClickListener;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

import de.leuphana.webshop.backend.Shop;
import de.leuphana.webshop.backend.User;
import de.leuphana.webshop.backend.model.Cart;
import de.leuphana.webshop.backend.model.CartItem;
import de.leuphana.webshop.backend.model.Work;
import de.leuphana.webshop.backend.service.WorkService;

@Title("Shop")
@Theme("valo")
public class WebshopUI extends UI {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7492268359434716545L;

	Shop shop = Shop.getInstance();

	User admin;

	WorkService service = shop.getWorkService();

	// i18n

	String language = "de";
	String country = "DE";

	Locale locale = new Locale(language, country);
	ResourceBundle captions = ResourceBundle.getBundle("de.leuphana.webshop.view.i18n.Captions", locale);

	Button switchLanguageButton = new Button();

	// main

	TextField filterTF = new TextField();

	Grid workList = new Grid();

	Button newWorkButton = new Button();
	Button loginButton = new Button();
	Button logoutButton = new Button();
	Button openCartButton = new Button();

	LoginForm loginForm = new LoginForm();
	ShowWorkForm showWorkForm = new ShowWorkForm();
	EditWorkForm editWorkForm = new EditWorkForm();

	// cart

	Button removeWorkButtom = new Button();
	Button orderButton = new Button();
	Button backButton = new Button();

	@Override
	protected void init(VaadinRequest request) {
		configureCartComponents();
//		buildCartLayout();
		configureMainComponents();
		buildMainLayout();
	}

	@SuppressWarnings("serial")
	private void configureMainComponents() {
		switchLanguageButton.addClickListener(e -> switchLanguage("en", "US"));

		newWorkButton.addClickListener(e -> editWorkForm.create());
		loginButton.addClickListener(e -> loginForm.setVisible(true));
		logoutButton.addClickListener(new ClickListener() {

			@Override
			public void buttonClick(ClickEvent event) {
				admin.setLoggedIn(false);
				buildMainLayout();
			}
		});
		openCartButton.addClickListener(e -> buildCartLayout());

		filterTF.setInputPrompt(captions.getString("filterTextField"));
		filterTF.addTextChangeListener(e -> refreshWorkList(e.getText()));

		workList.setContainerDataSource(new BeanItemContainer<>(Work.class));
		workList.setColumnOrder("subject", "numberOfPages", "year");
		workList.removeColumn("id");
		workList.removeColumn("type");
		workList.setSelectionMode(Grid.SelectionMode.SINGLE);
		workList.addSelectionListener(e -> showWorkForm.show((Work) workList.getSelectedRow()));
		workList.addSelectionListener(e -> editWorkForm.edit((Work) workList.getSelectedRow()));
		refreshWorkList();
	}

	private void buildMainLayout() {
		setCaptions();

		HorizontalLayout actions = new HorizontalLayout(filterTF, switchLanguageButton);

		// actions layout
		actions.setWidth("100%");
		filterTF.setWidth("100%");
		actions.setExpandRatio(filterTF, 1);

		// left side of layout
		VerticalLayout left = new VerticalLayout(actions, workList);
		left.setSizeFull();
		workList.setSizeFull();
		left.setExpandRatio(workList, 1);

		// main layout
		HorizontalLayout mainLayout = new HorizontalLayout(left);
		mainLayout.setSizeFull();
		mainLayout.setExpandRatio(left, 1);

		// style button
		refreshCartButtonCaption();
		openCartButton.addStyleName("primary");
		openCartButton.setIcon(FontAwesome.SHOPPING_CART);

		// add components for (not) logged in users
		if (admin.isLoggedIn()) {
			actions.addComponents(newWorkButton, logoutButton);
			mainLayout.addComponent(editWorkForm);
		} else {
			actions.addComponents(loginButton, openCartButton);
			mainLayout.addComponents(showWorkForm, loginForm);
		}

		setContent(mainLayout);
	}

	void setCaptions() {
		switchLanguageButton.setCaption(captions.getString("switchLanguageButton"));
		newWorkButton.setCaption(captions.getString("newWorkButton"));
		loginButton.setCaption(captions.getString("loginButton"));
		logoutButton.setCaption(captions.getString("logoutButton"));

		removeWorkButtom.setCaption(captions.getString("removeWorkButtom"));
		orderButton.setCaption(captions.getString("orderButton"));
		backButton.setCaption(captions.getString("backButton"));

		filterTF.setInputPrompt(captions.getString("filterTextField"));

		workList.getColumn("subject").setHeaderCaption(captions.getString("worklistColumnCaptionSubject"));
		workList.getColumn("numberOfPages").setHeaderCaption(captions.getString("worklistColumnCaptionNumberOfPages"));
		workList.getColumn("year").setHeaderCaption(captions.getString("worklistColumnCaptionYear"));
	}

	@SuppressWarnings("serial")
	private void configureCartComponents() {

		backButton.addClickListener(e -> buildMainLayout());
		orderButton.addClickListener(new ClickListener() {

			@Override
			public void buttonClick(ClickEvent event) {
				if (getSession().getAttribute("customerId") != null) {
					int customerId = (int) getSession().getAttribute("customerId");
					Cart cart = shop.getCartForCustomer(customerId);
					OrderLayout layout = new OrderLayout(cart, captions);
					setContent(layout);
				}
			}
		});

	}

	@SuppressWarnings("serial")
	void buildCartLayout() {
		VerticalLayout cartLayout = new VerticalLayout(backButton);

		Table cartItemsTable = new Table();
		cartItemsTable.setCaption(captions.getString("cartItemsTableCaption"));
		cartItemsTable.addContainerProperty(captions.getString("cartItemsTableQuantiyProperty"), Integer.class, null);
		cartItemsTable.addContainerProperty(captions.getString("cartItemsTableIDProperty"), Long.class, null);
		cartItemsTable.addContainerProperty(captions.getString("cartItemsTableSubjectProperty"), String.class, null);
		cartItemsTable.addContainerProperty("  ", String.class, null);

		if (getSession().getAttribute("customerId") != null) {
			int customerId = (int) getSession().getAttribute("customerId");
			Cart cart = shop.getCartForCustomer(customerId);

			if (cart != null && cart.getCartItems().isEmpty() == false) {
				int i = 1;
				for (CartItem cartItem : cart.getCartItems()) {
					cartItemsTable.addItem(
							new Object[] { cartItem.getQuantity(), cartItem.getWork().getId(),
									cartItem.getWork().getSubject(), captions.getString("cartItemsTableRemoveItem") },
							i++);
				}
				cartLayout.addComponents(cartItemsTable, orderButton);
			}
		}

		cartItemsTable.addItemClickListener(new ItemClickListener() {

			@Override
			public void itemClick(ItemClickEvent event) {
				Object id = event.getItem().getItemProperty("Id").getValue();
				Long workId = (Long) id;
				int customerId = (int) getSession().getAttribute("customerId");
				shop.getCartForCustomer(customerId).deleteCartItem(workId);
				buildCartLayout();
			}
		});

		cartItemsTable.setPageLength(cartItemsTable.size());

		setContent(cartLayout);
	}

	public void refreshCartButtonCaption() {
		if (getSession().getAttribute("customerId") != null) {
			int customerId = (int) getSession().getAttribute("customerId");

			Cart cart = shop.getCartForCustomer(customerId);

			int numberOfArticles = cart.getNumberOfArticles();

			openCartButton.setCaption(captions.getString("openCartButton") + " ( " + numberOfArticles + " )");
		} else {
			openCartButton.setCaption(captions.getString("openCartButton") + " ( 0 )");
		}
	}

	public void refreshLayout() {
		buildMainLayout();
	}

	void refreshWorkList() {
		refreshWorkList(filterTF.getValue());
	}

	private void refreshWorkList(String stringFilter) {
		workList.setContainerDataSource(new BeanItemContainer<>(Work.class, service.findAll(stringFilter)));
		editWorkForm.setVisible(false);
	}

	private void switchLanguage(String language, String country) {
		if (!captions.getLocale().getLanguage().equals(language)) {
			Locale locale = new Locale(language, country);
			changeCaptionsAndLayout(locale);
		} else {
			changeCaptionsAndLayout(locale);
		}
		buildCartLayout();
		buildMainLayout();
	}

	private void changeCaptionsAndLayout(Locale locale) {
		captions = ResourceBundle.getBundle("de.leuphana.webshop.view.i18n.Captions", locale);
		loginForm.captions = captions;
		showWorkForm.captions = captions;
		editWorkForm.captions = captions;
		loginForm.refreshCaptions();
		showWorkForm.refreshCaptions();
		editWorkForm.refreshCaptions();
	}

	@SuppressWarnings("serial")
	@WebServlet(urlPatterns = "/*")
	@VaadinServletConfiguration(ui = WebshopUI.class, productionMode = false)
	public static class WebshopUIServlet extends VaadinServlet {
	}

	public WebshopUI() {
		admin = new User();
		admin.setUsername("admin");
		admin.setPassword("admin");
	}

}

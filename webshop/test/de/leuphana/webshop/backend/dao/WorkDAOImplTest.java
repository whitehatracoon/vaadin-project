package de.leuphana.webshop.backend.dao;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import de.leuphana.webshop.backend.model.BachelorWork;
import de.leuphana.webshop.backend.model.Work;

public class WorkDAOImplTest {
	WorkDAO dao;

	@Before
	public void setUp() throws Exception {
		dao = new WorkDAOImpl();
	}

	@After
	public void tearDown() throws Exception {
		dao = null;
	}

	@Test
	public void findAllTest() {
		List<Work> works = dao.findAll();

		assertNotNull(works);
		assertFalse(works.isEmpty());
	}

	@Test
	public void findByIdTest() {
		Long id = (long) 1;
		Work work = dao.findById(id);

		assertNotNull(work.getId());
		assertEquals(id, work.getId());
	}

//	@Test
//	public void insertWorkTest() {
//		Work work = new Work();
//		work.setSubject("insterWorkViaDAO");
//		work.setNumberOfPages(1);
//		work.setYear("2016");
//
//		assertEquals(true, dao.insertWork(work));
//	}

	@Test
	public void updateWork() {
		Long id = (long) 1; // check first if possible

		BachelorWork oldWork = (BachelorWork) dao.findById(id);

		BachelorWork newWork = new BachelorWork();
		newWork.setId(id);
		newWork.setSubject("updateWorkViaDAO");
		newWork.setNumberOfPages(2);
		newWork.setYear("2017");
		newWork.setStudent("test student");
		newWork.setExaminer("test examiner");
		newWork.setSecondAuditor("test auditor");

		assertEquals(true, dao.updateWork(newWork));

		assertNotEquals(oldWork.getSubject(), newWork.getSubject());
		assertNotEquals(oldWork.getNumberOfPages(), newWork.getNumberOfPages());
		assertNotEquals(oldWork.getYear(), newWork.getYear());
	}

}

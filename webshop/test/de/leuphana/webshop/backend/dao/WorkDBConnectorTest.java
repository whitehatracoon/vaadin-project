package de.leuphana.webshop.backend.dao;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import de.leuphana.webshop.backend.dao.connector.WorkDBConnector;
import de.leuphana.webshop.backend.model.BachelorWork;
import de.leuphana.webshop.backend.model.SeminarWork;
import de.leuphana.webshop.backend.model.Work;

public class WorkDBConnectorTest {
	WorkDBConnector connector;

	@Before
	public void setUp() throws Exception {
		connector = new WorkDBConnector();
	}

	@After
	public void tearDown() throws Exception {
		connector = null;
	}

	@Test
	public void fetchWorksTest() {
		List<Work> works = connector.fetchWorks();

		assertNotNull(works);
		assertFalse(works.isEmpty());
	}

	@Test
	public void fetchWorkByIdTest() {
		Long id = (long) 1;
		Work work = connector.fetchWorkById(id);

		assertNotNull(work.getId());
		assertEquals(id, work.getId());
	}

	//@Test
	public void insertWorkTest() {
		SeminarWork work = new SeminarWork();
		work.setSubject("insertWorkViaDBC");
		work.setNumberOfPages(1);
		work.setYear("2016");
		work.setLecture("insert lecture");
		work.setLecturer("insert lecturer");

		connector.insertWork(work);
	}

	//@Test
	public void updateWorkTest() {
		Long id = (long) 1; // check first if possible

		BachelorWork oldWork = (BachelorWork) connector.fetchWorkById(id);

		BachelorWork newWork = new BachelorWork();
		newWork.setId(id);
		newWork.setSubject("updateWorkViaDBC");
		newWork.setNumberOfPages(1);
		newWork.setYear("2016");
		newWork.setStudent("test student");
		newWork.setExaminer("test examiner");
		newWork.setSecondAuditor("test auditor");

		connector.updateWork(newWork);

		newWork = (BachelorWork) connector.fetchWorkById(id);
		
		assertNotEquals(oldWork.getSubject(), newWork.getSubject());
		assertNotEquals(oldWork.getNumberOfPages(), newWork.getNumberOfPages());
		assertNotEquals(oldWork.getYear(), newWork.getYear());
	}

}

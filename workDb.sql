-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Erstellungszeit: 22. Okt 2016 um 09:12
-- Server-Version: 10.1.10-MariaDB
-- PHP-Version: 5.6.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `workDb`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `works`
--

CREATE TABLE `works` (
  `workId` bigint(20) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `subject` varchar(50) DEFAULT NULL,
  `numberOfPages` int(11) DEFAULT NULL,
  `year` varchar(4) DEFAULT NULL,
  `type` varchar(12) NOT NULL,
  `student` varchar(50) DEFAULT NULL,
  `examiner` varchar(50) DEFAULT NULL,
  `second auditor` varchar(50) DEFAULT NULL,
  `lecture` varchar(50) DEFAULT NULL,
  `lecturer` varchar(50) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `works`
--

INSERT INTO `works` (`workId`, `subject`, `numberOfPages`, `year`, `type`, `student`, `examiner`, `second auditor`, `lecture`, `lecturer`) VALUES
(1, 'updateWork', 1, '2016', 'BachelorWork', 'test', 'test examiner', 'test auditor', NULL, NULL),
(2, 'Seminaaaaa', 12, '2016', 'SeminarWork', NULL, NULL, NULL, '(Vor-)Lesung', 'Zent'),
(3, 'Testiii', 20, '2014', 'BachelorWork', 'Student', 'PPPP', 'ZZZ', NULL, NULL),
(4, 'Bachelor einfuegen', 20, '2016', 'BachelorWork', 'Student', 'Examiner', 'Auditor', NULL, NULL),
(5, 'insertWorkViaDBC', 1, '2016', 'null', 'NULL', 'NULL', 'NULL', 'insert lecture', 'insert lecturer'),
(7, 'Done', 34, '2070', 'BachelorWork', 'Master of Desaster', 'Impressed', 'Super Impressed', NULL, NULL),
(8, 'Retest finished status', 12, '2017', 'SeminarWork', 'NULL', 'NULL', 'NULL', 'Nice', 'Super nice');

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `works`
--
ALTER TABLE `works`
  ADD PRIMARY KEY (`workId`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `works`
--
ALTER TABLE `works`
  MODIFY `workId` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

# README #

small Vaadin Project

### Requirements ###

- Java SDK 1.8
- Mysql 5.6 oder newer
- Eclipse IDE for Java EE Developers Mars 2 or newer
- Tomcat 8.0 or newer
- Vaadin Eclipse plugin

### Install ###

*setup database*

1. CREATE DATABASE workDb;
2. CREATE USER 'workAdmin'@'localhost' IDENTIFIED BY ‚pass‘;
3. GRANT ALL ON workDb.* TO 'workAdmin'@'localhost';
4. run sql script in folder

*setup project*

1. Open eclipse
2. Import project into Eclipse
3. configure build path

- bind Java JRE
- add JUnit

4. add Tomcat Server
5. Go to settings -> ivy -> settings

- set the "ivy settings" path to 
${workspace_loc:webshop/ivysettings.xml}

6. right click on project folder 

- ivy -> resolve

### Run ###

1. Add project to Tomcat Server
2. Run Tomcat Server
3. Open http://localhost:8080/webshop/


### Contact ###

venance.cassens@whitehatracoons.de